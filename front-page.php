<?php get_header(); ?>

<div id="contents_wrap">
  <div id="contents">
    <section class="mainvisual" id="mainvisual">
      <div class="vegas-outer">
        <div style="display:none;" id="vegas-text">
          <p id="vegas-msg">
            <div id="text"></div>
          </p>
        </div>
      </div>
    </section>
    <!-- mainvisual -->
    
    <section class="top_menu pb">
      <div class="wrapper">
        <h3 class="pt pb"><span class="line">お車のことなら<span class="green">アイデリック</span>にお任せください！</span></h3>
        <ul class="grid_col3 tab3 sp1 cf enter-bottom">
          <li class="col photo_title"><a href="<?php bloginfo('url'); ?>/concept/">
            <figure class="cf">
              <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_photo1.jpg" alt="お店の取り組み"></div>
              <figcaption><span>お店の取り組み</span></figcaption>
            </figure>
            </a></li>
          <li class="col photo_title"><a href="<?php bloginfo('url'); ?>/sales">
            <figure class="cf">
              <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_photo2.jpg" alt="遠方のお客様へ"></div>
              <figcaption><span>遠方のお客様へ</span></figcaption>
            </figure>
            </a></li>
          <li class="col photo_title"><a href="<?php bloginfo('url'); ?>/sales#price">
            <figure class="cf">
              <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_photo3.jpg" alt="費用について"></div>
              <figcaption><span>費用について</span></figcaption>
            </figure>
            </a></li>
        </ul>
      </div>
    </section>
    <!-- top_menu -->
    
    <section class="bottom_menu">
        
        <div class="grid_ptn1 type1 cf enter-right">
            <div class="photo1">
            </div>
            <div class="text">
                <div class="enter-bottom">
                    <h3 class="mb_s">全国販売に対応しています</h3>
                    <p class="pt_s">当店では道内における遠方の方はもちろん、道外の方含めて全国販売を実施しております。追加の写真希望や細かいお問合せ、販売の流れなど、遠方ならではの対応もしっかりと行います！</p>
                    <p class="linkbtn1 mt"><a href="<?php bloginfo('url'); ?>/sales/">詳しくはコチラ</a></p>
                </div>
            </div>
            <!-- text -->
        </div>
        <!-- grid-ptn1 -->

        <div class="grid_ptn1 type2 cf enter-left">
            <div class="photo2">
            </div>
            <div class="text">
                <div class="enter-bottom">
                    <h3 class="mb_s">お客様の口コミをご紹介</h3>
                    <p class="pt_s">おかげさまで本当に多くのお客様から弊社の口コミを頂いております。下記のサイトよりそれぞれご確認頂けます！頂いたレビューはサービス向上の為、しっかりと対応させて頂きます。</p>
                    <div class="outer cf mt_s">
                        <div class="left">
                            <a target="_blank" href="https://www.goo-net.com/user_review/0302957/detail.html"><img src="<?php bloginfo('template_url'); ?>/images/goo.jpg" alt="グーネット" /></a>
                        </div>
                        <!-- left -->
                        <div class="right">
                            <a target="_blank" href="https://www.carsensor.net/shop/hokkaido/227309001/review/"><img src="<?php bloginfo('template_url'); ?>/images/carsensor.jpg" alt="カーセンサー" /></a>
                        </div>
                        <!-- right -->
                    </div>
                    <!-- outer -->
                </div>
            </div>
            <!-- text -->
        </div>
        <!-- grid-ptn1 -->

        <div class="grid_ptn1 type1 cf  enter-right">
            <div class="photo3">
            </div>
            <div class="text">
                <div class="enter-bottom">
                    <h3 class="mb_s">アイデリックのコンセプト</h3>
                    <p class="pt_s">アイデリックは全てのお客様に「安心」をお届けします。上質で格安なオートマ・４ＷＤの車輌を中心に取り揃え、お客様のお求め安い価格にてご提供させていただいております。</p>
                    <p class="linkbtn1 mt"><a href="<?php bloginfo('url'); ?>/concept/">詳しくはコチラ</a></p>
                </div>
            </div>
            <!-- text -->
        </div>
        <!-- grid-ptn1 -->

        <div class="grid_ptn1 type2 cf  enter-left">
            <div class="photo4">
            </div>
            <div class="text">
                <div class="enter-bottom">
                    <h3 class="mb_s">スタッフ紹介</h3>
                    <p class="pt_s">こちらでは当店のスタッフをご紹介しております。笑顔が見える店舗として、お客様を心よりお待ちしております。</p>
                    <p class="linkbtn1 mt"><a href="<?php bloginfo('url'); ?>/staff/">詳しくはコチラ</a></p>
                </div>
            </div>
            <!-- text -->
        </div>
        <!-- grid-ptn1 -->

        

    </section>
    
    <section class="line_bnr mt mb">
        <div class="wrapper">
            <a href="<?php bloginfo('url'); ?>/sales#line"><img class="enter-bottom" src="<?php bloginfo('template_url'); ?>/images/index_line_bnr.jpg" alt="アイデリック公式LINE" /></a>
        </div>
    </section>



  </div>
  <!--contents --> 
</div>
<!--contents_wrap -->
<?php get_footer(); ?>
