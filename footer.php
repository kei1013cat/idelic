<div class="footer-tel sp">
    <p><a href="tel:0117763222"><span class="text">今すぐ電話をする</span><span class="num">011-776-3222</span></a></p>
    <p class="pb">受付時間：10:00～20:00</p>
</div>

<footer>
  <nav>
  <ul class="cf">
  <li><a href="<?php bloginfo('url'); ?>/company/">会社情報</a></li>
  <li><a href="<?php bloginfo('url'); ?>/sales/">全国販売</a></li>
  <li><a target="_blank" href="https://www.goo-net.com/usedcar_shop/0302957/stock.html">在庫</a></li>
  <li><a href="<?php bloginfo('url'); ?>/concept/">コンセプト</a></li>
  <li><a href="<?php bloginfo('url'); ?>/staff/">スタッフ</a></li>
  <li><a target="_blank" href="https://www.goo-net.com/user_review/0302957/detail.html">口コミ</a></li>
  </ul>
  </nav>

	<div class="wrapper cf">
    <div class="left">
      <h2 class="cf">
        <a class="logo" href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.png"></a>
        <div class="comp">
          <p>新車 中古車販売 整備 板金 車検 パーツ販売取付</p>
          <h3><strong>有限会社アイデリック</strong></h3>
        </div>
      </h2>
      <address>〒002-8054　北海道札幌市北区篠路町拓北25-184</address>
      <p class="time">定休日：火曜日、お盆、年末年始</p>
    </div><!-- left -->

    <div class="right">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2910.786681258516!2d141.39915591500593!3d43.15100857914168!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f20.1!3m3!1m2!1s0x5f0b2f8a7c71a95d%3A0x98737d94640fe234!2z44CSMDAyLTgwNTQg5YyX5rW36YGT5pyt5bmM5biC5YyX5Yy656-g6Lev55S65ouT5YyX77yS77yV4oiS77yR77yY77yU!5e0!3m2!1sja!2sjp!4v1548722588251" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- right -->

  </div>
  <!-- wrapper -->
  <p class="copy">Copyright &copy; idelic All Rights Reserved.</p>

</footer>
<p id="page_top"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/pagetop.png" alt="pagetop"></a></p>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
<?php wp_footer(); ?>
</main>
</div><!--outer -->
<script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script> 
</body>

</html>