<?php

//-------------------------------------------
// スマホならtrue, タブレット・PCならfalseを返す
//-------------------------------------------

global $is_mobile;
$is_mobile = false;

$useragents = array(
 'iPhone',          // iPhone
 'iPod',            // iPod touch
 'Android',         // 1.5+ Android
 'dream',           // Pre 1.5 Android
 'CUPCAKE',         // 1.5+ Android
 'blackberry9500',  // Storm
 'blackberry9530',  // Storm
 'blackberry9520',  // Storm v2
 'blackberry9550',  // Storm v2
 'blackberry9800',  // Torch
 'webOS',           // Palm Pre Experimental
 'incognito',       // Other iPhone browser
 'webmate'          // Other iPhone browser
 );
 $pattern = '/'.implode('|', $useragents).'/i';
 if( preg_match($pattern, $_SERVER['HTTP_USER_AGENT'])){
	 $is_mobile = preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
 }

function is_mobile(){
	global $is_mobile;
	return $is_mobile;
	//return false;
}
function is_pc(){
	if(is_mobile()){
		return false;
	}else{
		return true;
	}
}
function is_ipad(){
	if(strpos($_SERVER['HTTP_USER_AGENT'],'iPad')){
		return true;
	}else{
		return false;
	}
}
function mobile_img(){
	if (is_mobile()) {
		echo "_sp";
	}
}



//--------------------------------------------------------------------------------------------
// エディター関連
//--------------------------------------------------------------------------------------------

//-------------------------------------------
// 使用しないメニューを非表示にする
//-------------------------------------------
function remove_admin_menus() {
 
    // level10以外のユーザーの場合
//    if (!current_user_can('level_10')) {
        global $menu;

        // unsetで非表示にするメニューを指定
//        unset($menu[2]);        // ダッシュボード
//        unset($menu[5]);        // 投稿
//        unset($menu[10]);       // メディア
//        unset($menu[20]);       // 固定ページ
        unset($menu[25]);       // コメント
//        unset($menu[60]);       // 外観
//        unset($menu[65]);       // プラグイン
//        unset($menu[70]);       // ユーザー
//        unset($menu[75]);       // ツール
//        unset($menu[80]);       // 設定
//    }
}
add_action('admin_menu', 'remove_admin_menus');


//-------------------------------------------
//プレビューボタン非表示
//-------------------------------------------
add_action('admin_print_styles', 'admin_preview_css_custom');
function admin_preview_css_custom() {
   echo '<style>#preview-action {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
}

//-------------------------------------------
//記事画像パスを相対パスで利用する
//-------------------------------------------

function imagepassshort($arg) {
$content = str_replace('"images/', '"' . get_bloginfo('template_directory') . '/images/', $arg);
return $content;
}
add_action('the_content', 'imagepassshort');


//-------------------------------------------
//　投稿の自動整形を無効
//-------------------------------------------
//出力側の変換機能を無効化
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_content', 'wptexturize' );

// ビジュアルエディタの変換も無効
function override_mce_options( $init_array ) {
    global $allowedposttags;

    $init_array['valid_elements']          = '*[*]';
    $init_array['extended_valid_elements'] = '*[*]';
    $init_array['valid_children']          = '+a[' . implode( '|', array_keys( $allowedposttags ) ) . ']';
    $init_array['indent']                  = true;
    $init_array['wpautop']                 = false;
    $init_array['force_p_newlines']        = false;

    return $init_array;
}

add_filter( 'tiny_mce_before_init', 'override_mce_options' );

//-------------------------------------------
// エディタをビジュアルにする
//-------------------------------------------
//add_filter('wp_default_editor', create_function('', 'return "tinymce";'));


//-------------------------------------------
//管理画面の「見出し１」等を削除する
//-------------------------------------------

function custom_editor_settings( $initArray ){
$initArray['block_formats'] = "段落=p; 見出し1=h3; 見出し2=h4;";
return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );


//-------------------------------------------
//投稿部分表示カスタマイズ
//-------------------------------------------
function change_post_menu_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'お知らせ';
  $submenu['edit.php'][5][0] = 'お知らせ一覧';
  $submenu['edit.php'][10][0] = '新規作成';
  //$submenu['edit.php'][16][0] = 'タグ';
  //echo ”;
}
function change_post_object_label() {
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'お知らせ';
  $labels->singular_name = 'お知らせ';
  $labels->add_new = _x('新規作成', '新着情報');
  $labels->add_new_item = '新しい情報';
  $labels->edit_item = '情報の編集';
  $labels->new_item = '新しい情報';
  $labels->view_item = '新着情報を表示';
  $labels->search_items = '新着情報検索';
  $labels->not_found = '新着情報が見つかりませんでした';
  $labels->not_found_in_trash = 'ゴミ箱の新着情報にも見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

//-------------------------------------------
//サムネイルサイズを追加
//-------------------------------------------

function thumbAdd() {
add_theme_support( 'post-thumbnails' ); //テーマをサムネイル表示に対応させる
add_image_size( 'interview_list', 480,257); //インタビュー一覧用の画像サイズ
}
add_action( 'after_setup_theme', 'thumbAdd' );

//--------------------------------------------------------------------------------------------
// frontend
//--------------------------------------------------------------------------------------------

//-------------------------------------------
//ページャー
//-------------------------------------------
function bmPageNaviGallery() {
  global $wp_rewrite;
  global $wp_query;
  global $paged;
 
  $paginate_base = get_pagenum_link(1);
  if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
    $paginate_format = '';
    $paginate_base = add_query_arg('page', '%#%');
  } else {
    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '') .
    untrailingslashit('page/%#%', 'paged');
    $paginate_base .= '%_%';  
  }
  $cat = "";
  if(!empty($_GET['cat'])){
	$cat = "&cat=".htmlspecialchars($_GET['cat'], ENT_QUOTES);
  }
  //$paginate_base = "./page/%#%".$cat;
  $result = paginate_links( array(
   // 'base' => $paginate_base,
    'format' => $paginate_format,
    'total' => $wp_query->max_num_pages,
    'mid_size' => 4,
    'prev_text' => '&lt;&lt;',
    'next_text' => '&gt;&gt;',
    'current' => ($paged ? $paged : 1),
  ));
 
  return $result;
}


//-------------------------------------------
// 郵便番号自動
//-------------------------------------------
function ajaxzip3_scripts() {
    wp_enqueue_script( 'ajaxzip3-script', get_bloginfo('template_directory') . '/js/ajaxzip3.js', array( 'jquery' ), '20140807', true );
}
add_action( 'wp_enqueue_scripts', 'ajaxzip3_scripts' );


//ショートコードを使ったphpファイルの呼び出し方法
function my_php_Include($params = array()) {
 extract(shortcode_atts(array('file' => 'default'), $params));
 ob_start();
 include(STYLESHEETPATH . "/$file.php");
 return ob_get_clean();
}
add_shortcode('myphp', 'my_php_Include');


?>