<?php
  $url = $_SERVER['REQUEST_URI'];
?>
<div class="<?php echo $post->post_name; ?> fead" id="pagetitle-photo">
</div>
<!-- page_title -->
<div class="pagetitle-text">
    <div class="wrapper enter-title">
        <h2>
        <?php if($post->post_name=="concept"):?>
            Concept<span class="small">お店の取り組み</span>
        <?php elseif($post->post_name=="sales"):?>
            Sales<span class="small">全国販売について</span>
        <?php elseif($post->post_name=="staff"):?>
            Staff<span class="small">スタッフについて</span>
        <?php elseif($post->post_name=="company"):?>
            Company<span class="small">会社概要</span>
        <?php endif; ?>
        </h2>
    </div>
    <!-- wrapper -->
</div>
<!-- pagetitle-text -->
