<section class="company pt_l pb_l">
  <div class="wrapper">
    <table class="style01 mb mt_s">
      <tbody>
        <tr>
          <th>社名</th>
          <td>有限会社アイデリック</td>
        </tr>
        <tr>
          <th>設立</th>
          <td>平成17年11月11日</td>
        </tr>
        <tr>
          <th>本社</th>
          <td>北海道札幌市北区篠路町拓北２５－１８４</td>
        </tr>
        <tr>
          <th>電話</th>
          <td>011-776-3222</td>
        </tr>
        <tr>
          <th>FAX</th>
          <td>011-776-3223</td>
        </tr>
        <tr>
          <th>定休日</th>
          <td>火曜日、お盆、年末年始</td>
        </tr>
        <tr>
          <th>代表者</th>
          <td>濱田真亮</td>
        </tr>
        <tr>
          <th>資本金</th>
          <td>300万円</td>
        </tr>
        <tr>
          <th>事業内容</th>
          <td>
            中古車販売・板金・整備・レンタカー<br>
            1.自動車販売、修理<br>
            2.原動機付自転車・自動二輪車の販売、修理及びリース<br>
            3.音響機器・電気通信機器・家庭用電気製品の販売<br>
            4.カー用品並びに日曜雑貨の販売<br>
            5.タイヤ並びにゴム工業製品の販売<br>
            6.損害保険代理業及び自動車損害賠償法に基づく保険代理業<br>
            7.古物売買並びにその受託販売<br>
            8.イベント企画<br>
            9.前各号に附帯する一切の業務
          </td>
        </tr>
        <tr>
          <th>取引銀行</th>
          <td>北洋銀行・札幌信用金庫</td>
        </tr>
        <tr>
          <th>従業員数</th>
          <td>5名</td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- wrapper --> 

</section>

<section class="access bg_green pt">
    <div class="wrapper">
        <h3 class="headline mb">アクセス</h3>
        <p>場所に迷ったらお電話011-776-3222ください！<br>あいの里教育大駅 南口までお越し頂けましたらお迎えにいくことも可能です。</p>
    </div>
    <div class="map pt">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7998.433466829023!2d141.39811827394797!3d43.152064679119704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f33.1!3m3!1m2!1s0x5f0b2f8a7c71a95d%3A0x98737d94640fe234!2z44CSMDAyLTgwNTQg5YyX5rW36YGT5pyt5bmM5biC5YyX5Yy656-g6Lev55S65ouT5YyX77yS77yV4oiS77yR77yY77yU!5e0!3m2!1sja!2sjp!4v1549437808038" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>

<div class="obi"></div>

