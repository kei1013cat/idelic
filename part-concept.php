<section class="<?php echo $post->post_name; ?> top-title">
    <h3><span class="line">経験豊富なスタッフ・充実した品揃え・購入後のカーライフもサポート</span></h3>
    <p>アイデリックはすべてのお客様に<span class="big"><span class="green">「安心」</span>をお届けします。</span></p>
</section>

<section class="concept">
    <div id="contents_outer" class="cf">
    <div id="main_contents">
        <h3 class="headtitle"><img src="<?php bloginfo('template_url'); ?>/images/concept_headtitle.svg" alt="アイデリックの「ここ」がポイント" /><span class="num">1</span></h3>
        <div class="outer cf">
            <div class="left">
                <div class="photo fead">
                    <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg"  /></div>
                    <img src="<?php bloginfo('template_url'); ?>/images/concept_photo1.jpg"  />
                </div>
            </div>
            <!-- left -->
            <div class="right">
                <h4>安心の無料保証・整備付き販売、安心の総額表示店</h4>
                <p>上質で格安なオートマ・４ＷＤの車輌を中心に取り揃え、お客さんのお求め安い価格にてご提供させていただいております。<br>当社には経験豊富なスタッフが、確かな目で厳選し、仕入れた車輌しかございません。 しかも全車に安心の無料保証・整備をお付けしております。御車のことでしたら何でも御相談ください！</p>
            </div>
            <!-- right -->
        </div>
        <!-- outer -->

        <h3 class="headtitle mt_l"><img src="<?php bloginfo('template_url'); ?>/images/concept_headtitle.svg" alt="アイデリックの「ここ」がポイント" /><span class="num">2</span></h3>
        <div class="outer cf">
            <div class="left">
                <div class="photo fead">
                    <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg"  /></div>
                    <img src="<?php bloginfo('template_url'); ?>/images/concept_photo2.jpg"  />
                </div>
            </div>
            <!-- left -->
            <div class="right">
                <h4>寛げる空間の商談スペース</h4>
                <p>お車の事ならお気軽にご相談下さい。お子様連れ、ペット連れのお客様も歓迎です！経験豊富で親身なスタッフが親切丁寧対応いたします。<br>お客様に安心して御車をお買い求めいただく為に、些細な事でも御相談いただけますように心がけております。大事な御車の事を納得ゆくまで、とことん御相談ください。　</p>
            </div>
            <!-- right -->
        </div>
        <!-- outer -->

        <h3 class="headtitle mt_l"><img src="<?php bloginfo('template_url'); ?>/images/concept_headtitle.svg" alt="アイデリックの「ここ」がポイント" /><span class="num">3</span></h3>
        <div class="outer cf">
            <div class="left">
                <div class="photo fead">
                    <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg"  /></div>
                    <img src="<?php bloginfo('template_url'); ?>/images/concept_photo3.jpg"  />
                </div>
            </div>
            <!-- left -->
            <div class="right">
                <h4>広々、自社工場・ショールーム完備</h4>
                <p>広々とした自社工場・ショールームを完備しております。ショールームでは、天候に左右されず、ゆっくりと気になる御車をご覧になっていただけます。自社工場も完備しておりますので、お客様の御車をスタッフがいつでも点検整備いたします。お近くをお通りの際は、いつでもお気軽にお立ち寄りください。<br>もちろん納車前の美装もお任せ下さい！ピカピカにして納車致します！搬送車で全道どこでも納車致します！</p>
            </div>
            <!-- right -->
        </div>
        <!-- outer -->

        <h3 class="headtitle mt_l"><img src="<?php bloginfo('template_url'); ?>/images/concept_headtitle.svg" alt="アイデリックの「ここ」がポイント" /><span class="num">4</span></h3>
        <div class="outer cf">
            <div class="left">
                <div class="photo fead">
                    <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg"  /></div>
                    <img src="<?php bloginfo('template_url'); ?>/images/concept_photo4.jpg"  />
                </div>
            </div>
            <!-- left -->
            <div class="right">
                <h4>親切・丁寧な接客</h4>
                <p>車の整備はもちろん、お客様のカーライフをしっかりサポートします！納車後のアフターフォローや御車のことなら何でもお任せ下さい。<br>お客様のご来店をスタッフ一同、心よりお待ちしております！　</p>
            </div>
            <!-- right -->
        </div>
        <!-- outer -->

        <h3 class="headtitle mt_l"><img src="<?php bloginfo('template_url'); ?>/images/concept_headtitle.svg" alt="アイデリックの「ここ」がポイント" /><span class="num">5</span></h3>
        <div class="outer cf">
            <div class="left">
                <div class="photo fead">
                    <div class="tape enter-left"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg"  /></div>
                    <img src="<?php bloginfo('template_url'); ?>/images/concept_photo5.jpg"  />
                </div>
            </div>
            <!-- left -->
            <div class="right">
                <h4>高価買い取りに自信あり！</h4>
                <p>コンパクトカーから輸入車まで幅広く買取りしております！もちろん下取りも歓迎！搬送車もありますので、ご自宅までお車も取りに伺えますのでご安心下さい！！</p>
            </div>
            <!-- right -->
        </div>
        <!-- outer -->
    </div>
    <!-- left_contents -->
	<?php get_sidebar(); ?>
    </div>
    <!-- main_contents -->
<div class="obi"><img src="<?php bloginfo('template_url'); ?>/images/concept_obi.jpg"  /></div>
</section>
<!-- concept -->