<section id="sidebar">
<ul class="cf">
    <li class="mb_s"><a href="<?php bloginfo('url'); ?>/company/"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr1.jpg" alt="アイデリック株式会社"/></a></li>
    <li class="mb_s pt_s"><a href="<?php bloginfo('url'); ?>/sales/"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr2.jpg" alt="全国販売について"/></a></li>
    <li class="mb_s pt_s"><a target="_blank" href="https://www.goo-net.com/usedcar_shop/0302957/stock.html"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr3.jpg" alt="在庫情報"/></a></li>
    <li class="mb_s pt_s"><a href="<?php bloginfo('url'); ?>/concept/"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr4.jpg" alt="アイデリックについてご紹介します！ |　コンセプト"/></a></li>
    <li class="pt_s"><a target="_blank" href="https://www.goo-net.com/user_review/0302957/detail.html"><img src="<?php bloginfo('template_url'); ?>/images/sidebar_bnr5.jpg" alt="高評価レビュー獲得! | ありがとうございます！"/></a></li>
</ul>
</section>
