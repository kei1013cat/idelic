<section class="<?php echo $post->post_name; ?> top-title">
    <div class="sp sp-photo">
        <img class="img_center pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/sales_title_sp.jpg"  />
    </div>
    <h3>アイデリックでは全国販売が可能です！<br>遠方の方もご相談ください。</h3>
    <div class="outer">
        <h4>実際にお車を見れない方でも安心してお任せ下さい！</h4>
        <p>諸事情や遠方の為、実際にお車を見れない方も安心してご購入できるよう、<br>
        当店では購入までの流れをHP上で公開しております。<br>
        お届け先までのおおよその陸送費用もページ内にございますので、<br>
        是非購入検討の際の参考までにご確認下さい。</p>
    </div>
</section>

<section class="flow bg_gray pt pb">
    <div id="contents_outer" class="cf">
    <div id="main_contents">
        <h3 class="headline mb">納車までの流れ</h3>
        <dl>
            <dt class="cf"><span class="num">1</span><span class="text">お見積もり依頼</span></dt>
            <dd>まずはネット上にてお見積もりの依頼をして頂き、<br>
            対象のお車のお見積もりをお客様にお送りします。<br>
            もちろんお電話でも構いません。気軽にお問合せ下さい。</dd>
        </dl>
        <img class="img_center pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/under_arrow.svg"  />
        <dl>
            <dt class="cf"><span class="num">2</span><span class="text">購入の検討・ご不明点の解消</span></dt>
            <dd>実際に価格をご覧になられた上で、現状の公開情報でわからない部分や、<br>
                その他ご質問などをお受けいたします。こちらLINEでの対応も可能です。</dd>
        </dl>
        <img class="img_center pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/under_arrow.svg"  />
        <dl>
            <dt class="cf"><span class="num">3</span><span class="text">契約・必要書類の郵送</span></dt>
            <dd>お客様にご納得頂いた上で、こちら購入の場合に注文書を発行致します。<br>
            こちらを郵送、またはFAX・メールなどでお送りしました後に、<br>
            お客様の方でこちらの書類の記入を行い、返送して頂きます。<br>
            ※その際に契約頭金として前金一律30,000円頂戴しております。</dd>
        </dl>
        <img class="img_center pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/under_arrow.svg"  />
        <dl>
            <dt class="cf"><span class="num">4</span><span class="text">納車の手続き・書類郵送のお願い</span></dt>
            <dd>無事ご契約書類の内容確認、ローンなどがある場合はこちらの審査の完了を経て、<br>
            お客様の元へ購入車両をお送り致します。その際には車庫証明などの書類も必要となりますが、こちらも納車までの流れとしてしっかりとご説明致しますので、ご安心下さい。</dd>
        </dl>

    </div>
    <div class="pc">
    <!-- left_contents -->
    <?php if(is_pc()): ?>
        <?php get_sidebar(); ?>
    <?php endif; ?>
    </div>
    </div>
    <!-- main_contents -->
</section>
<!-- flow -->

<section class="price" id="price">
    <div class="wrapper">
        <div id="main_contents" class="col1">
            <h3 class="headline mb mt">陸送料金について</h3>
        </div>
        <div class="outer">
            <div id="main_contents" class="col1">
                <img class="img_center pt_s pb_s" src="<?php bloginfo('template_url'); ?>/images/sales_map.svg"  />
            </div>
            <div class="msgbox">
                引取りからお届けまで３日間～10日間が目安です。詳細はお問合せください。札幌管轄外の場合は陸送費用のほかに別途登録手数料がかかります。<br>
                <span class="red">普通車:全道一律5,250円 / 道外一律:31500円 / 軽自動車:道内一律5,250円 / 道外一律:10,500円</span><br>
                遠方のため当店へご来店できないお客様へは別途有料保証もご用意しております。最大３年間の保証でニーズに合わせてお選びできます。詳細はお問合せください。
            </div>
        </div>
        <!-- outer -->
    </div>
</section>
<!-- price -->

<section class="line" id="line">
<div class="wrapper">
    <div id="main_contents" class="col1">
        <div class="title pt pb">
            <h3 class="pt_s"><img class="img_center" src="<?php bloginfo('template_url'); ?>/images/sales_line_title.svg"  /></h3>
            <h4 class="pb_s mb_s">LINEトークで直接お問い合わせ可能！キャンペーン情報もGET！</h4>
            <ul class="cf pt_s">
                <li>遠方のお客様も安心！</li>
                <li>動画や写真がやり取りできる！</li>
                <li>トークで気軽に相談できる</li>
            </ul>
        </div>
        <!-- title -->
    </div>
</div>
</section>
<!-- line -->

<section class="line_cont pb_l">
<div class="wrapper">
    <div id="main_contents" class="col1">
        <img class="img_center pt_l pb" src="<?php bloginfo('template_url'); ?>/images/sales_line_cont.svg"  />
        <h3>友だち追加方法</h3>
        <ul class="cf mb_s">
            <li>
              <dl class="no1">
                <dt>
                    <p class="num">1</p>
                    <p class="text">スマートフォンの方は<br class="pc">こちらをクリック！</p>
                </dt>
                <dd class="btn"><a href="#"><img class="img_center" src="<?php bloginfo('template_url'); ?>/images/line_add_btn.svg"  /></a></dd>
              </dl>
            </li>
            <li>
              <dl class="no2">
                <dt>
                    <p class="num">2</p>
                    <p class="text">LINEの友だち追加から<br class="pc">【ID検索】で登録</p>
                </dt>
                <dd class="line_id">××××××××××</dd>
              </dl>
            </li>
            <li>
              <dl class="no3">
                <dt>
                    <p class="num">3</p>
                    <p class="text">LINEの友だち追加から<br class="pc">【QRコード】で登録</p>
                </dt>
                <dd class="qr"><img height="60" border="0" alt="LINE | QRコード" src="<?php bloginfo('template_url'); ?>/images/line_qr.svg"></dd>
              </dl>
            </li>
      </ul>
    </div>
</div>
</section>
<!-- line_cont -->
<div class="obi"></div>
<?php if(is_mobile()): ?>
<?php get_sidebar(); ?>
<?php endif; ?>
