<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo '  '; }
 bloginfo('name'); ?>
    </title>
    <meta name="keyword" content="" />
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <?php if(is_pc()):?>
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
    <![endif]-->
    <?php endif; ?>
    <?php //以下user設定 ?>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-3.3.1.min.js"></script>

    <!-- scrollreveal -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>

    <!-- accordion -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/accordion.js"></script>
    <!-- smoothScroll -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
    <!-- pulldown -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>

    <!-- matchHeight -->
    <script src="<?php bloginfo('template_url'); ?>/js/match-height/jquery.matchHeight.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/matchHeight_userdf.js"></script>

    <!-- rwdImageMaps -->
    <script src="<?php bloginfo('template_url'); ?>/js/rwdImageMaps/jquery.rwdImageMaps.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/rwdImageMaps_userdf.js"></script>

    <!-- drawer -->
    <script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/drawer/dist/css/drawer.min.css">
    <script src="<?php bloginfo('template_url'); ?>/js/drawer/dist/js/drawer.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/drawer_userdf.js"></script>



    <script type="text/javascript">
        jQuery(function($) {
            // ドロワーメニューが開いたとき
            $('.drawer').on('drawer.opened', function() {
                $('#menu-text').text('CLOSE');
            });
            // ドロワーメニューが閉じたとき
            $('.drawer').on('drawer.closed', function() {
                $('#menu-text').text('MENU');
            });
        });

    </script>

    <script>
        jQuery(function() {
            $('#loading_wrap').delay(100).fadeOut("slow");
        });

    </script>

    <!-- スライダー(vegas)-->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/vegas/vegas.css">
    <script src="<?php bloginfo('template_url'); ?>/js/vegas/vegas.js"></script>
    <script>
        $(function() {
            $('#mainvisual').vegas({
                slides: [{
                        src: 'wp-content/themes/idelic/images/slider1<?php echo mobile_img(); ?>.jpg?v=20190215',
                        msg: '<h3>IDelic<span class="small">アイデリック</span></h3><p>新車・中古車販売。整備・板金・車検・パーツ販売取付。車のことなら何でもお任せ下さい。</p>'
                    },
                    {
                        src: 'wp-content/themes/idelic/images/slider2.jpg?v=20190215',
                        msg: '<h3>IDelic<span class="small">アイデリック</span></h3><p>新車・中古車販売。整備・板金・車検・パーツ販売取付。車のことなら何でもお任せ下さい。</p>'
                    },
                    {
                        src: 'wp-content/themes/idelic/images/slider3<?php echo mobile_img(); ?>.jpg?v=20190215',
                        msg: '<h3>IDelic<span class="small">アイデリック</span></h3><p>新車・中古車販売。整備・板金・車検・パーツ販売取付。車のことなら何でもお任せ下さい。</p>'
                    },
                    {
                        src: 'wp-content/themes/idelic/images/slider4<?php echo mobile_img(); ?>.jpg?v=20190215',
                        msg: '<h3>IDelic<span class="small">アイデリック</span></h3><p>新車・中古車販売。整備・板金・車検・パーツ販売取付。車のことなら何でもお任せ下さい。</p>'
                    },
                ],
                overlay: false,
                transition: 'flash',
                animation: 'fead2',
                transitionDuration: 1500,
                delay: 7000,
                animationDuration: 18000,
                timer: false
            });

        });

    </script>

    <!-- etc -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/product_apply.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/ajaxzip3.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/ajaxzip3_userdf.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/privacyarea_proc.js"></script>

    <?php if(is_front_page()): ?>

    <!-- slick -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick-theme.css" type="text/css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/slick_userdf.js"></script>

    <?php endif; ?>


    <?php wp_head(); ?>
</head>
<?php
  $body_id = "";
  $body_class = "";
  if ( is_front_page() ) {
    $body_id = ' id="page_index"';
  }else if ( is_page() ) {
    if($post -> post_parent == 0 ){
        $body_id = ' id="page_'.$post->post_name.'"';
    }else{
        $ancestors =  $post-> ancestors;
        foreach($ancestors as $ancestor){
            $body_id = ' id="page_'.get_post($ancestor)->post_name.'"';
            break;
        }
    }
    $body_class = ' subpage';
    if( $post->post_parent){
        $body_class = ' '.$post->post_name;
    }
  }else if (  is_single() ) {
    $body_id = ' id="page_single"';
   $body_class = " subpage ".get_post_type( $post );
  }else if ( is_archive() ) {
    $body_id = ' id="page_archive"';
    $body_class = " subpage ".get_post_type( $post );
  }else if ( is_404() ) {
    $body_id = ' id="page_404"';
    $body_class = ' subpage';
  }
?>
<body<?php echo $body_id; ?> class="drawer drawer--top<?php echo $body_class; ?>">
    <div id="loading_wrap"></div>
    <div id="outer">
        <header>
            <div class="pcmenu cf">
                <div class="header_top cf">
                    <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="idelic | アイデリック" /></a></h1>
                    <nav>
                        <ul class="cf">
                            <li><a href="<?php bloginfo('url'); ?>/company/"><span>会社情報</span></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/sales/"><span>全国販売</span></a></li>
                            <li><a href="javascript:void(0)"><span>在庫</span></a>
                                <ul class="drop">
                                    <li><a target="_blank" href="https://www.goo-net.com/usedcar_shop/0302957/stock.html">グーネット</a></li>
                                    <li><a target="_blank" href="https://www.carsensor.net/shop/hokkaido/227309001/stocklist/">カーセンサー</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php bloginfo('url'); ?>/concept/"><span>コンセプト</span></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/staff/"><span>スタッフ</span></a></li>
                            <li><a href="javascript:void(0)"><span>口コミ</span></a>
                                <ul class="drop">
                                    <li><a target="_blank" href="https://www.goo-net.com/user_review/0302957/detail.html">グーネット</a></li>
                                    <li><a target="_blank" href="https://www.carsensor.net/shop/hokkaido/227309001/review/">カーセンサー</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- header_top -->
                <div class="header_bottom cf">
                    <div class="box">
                        <div class="tel">
                            <img src="<?php bloginfo('template_url'); ?>/images/header_telmark.png" alt="" />011-776-3222
                        </div>
                        <!-- tel -->
                        <div class="time">
                            受付時間：10:00～20:00
                        </div>
                        <!-- time -->
                    </div>
                    <!-- box -->

                    <div class="line"></div>

                </div><!-- header_bottom -->
            </div>
            <!-- pcmenu -->
            <div class="spmenu drawermenu" role="banner" id="top">
                <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="idelic | アイデリック" /></a></h1>
                <button type="button" class="drawer-toggle drawer-hamburger">
                    <span class="sr-only">toggle navigation</span>
                    <span class="drawer-hamburger-icon"></span>
                    <div id="menu-text" class="text">MENU</div>
                </button>
                <nav class="drawer-nav" role="navigation">

                    <div class="nav-top">
                        <ul class="cf">
                            <li><a target="_blank" href="tel:0117763222"><img src="<?php bloginfo('template_url'); ?>/images/header_tel_icon.svg" alt="" /><span class="text">電話をかける</span></a></li>
                            <li><a target="_blank" href="https://goo.gl/maps/ar3hMS39MPz"><img src="<?php bloginfo('template_url'); ?>/images/header_map_icon.svg" alt="" /><span class="text">Google Maps</span></a></li>
                        </ul>
                    </div>

                    <div class="inner">
                        <h3 class="sp-title">MENU</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/company/">会社情報</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/sales/">全国販売</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/concept/">コンセプト</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/staff/">スタッフ</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/sales/#price">費用について</a></li>
                        </ul>
                        <h3 class="sp-title">在庫情報</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" target="_blank" href="https://www.goo-net.com/usedcar_shop/0302957/stock.html">グーネット</a></li>
                            <li class="color"><a class="drawer-menu-item" target="_blank" href="https://www.carsensor.net/shop/hokkaido/227309001/review/">カーセンサー</a></li>
                        </ul>
                        <h3 class="sp-title">口コミ</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" target="_blank" href="https://www.goo-net.com/user_review/0302957/detail.html">グーネット</a></li>
                            <li class="color"><a class="drawer-menu-item" target="_blank" href="https://www.carsensor.net/shop/hokkaido/227309001/stocklist/">カーセンサー</a></li>
                        </ul>
                    </div>
                    <!-- inner -->
                </nav>
            </div>
            <!-- spmenu -->

        </header>
        <main role="main">
