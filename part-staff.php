<section class="staff_list pt_l pb_l">
    <div class="wrapper pb_l">
        <dl class="cf enter-bottom">
            <dt class="matchheight"><img src="<?php bloginfo('template_url'); ?>/images/staff_photo1.jpg"  /></dt>
            <dd class="matchheight">
                <h3>濱田<span class="job">総務担当</span></h3>
                <p>総務担当の濱田です！<br>
                元気いっぱいに店舗サービス向上に取り組んでおります！
                不安な部分はお気軽にご相談下さい！</p>
            </dd>
        </dl>
        <dl class="cf mt_l enter-bottom">
            <dt class="matchheight"><img src="<?php bloginfo('template_url'); ?>/images/staff_photo2.jpg"  /></dt>
            <dd class="matchheight">
                <h3>岡部<span class="job">整備・車検担当</span></h3>
                <p>整備・車検担当の岡部です！<br>
                お車のメンテナンスや車検のことならお任せください！<br>
                ご気軽に、ご相談ください！！</p>
            </dd>
        </dl>
        <dl class="cf mt_l enter-bottom">
            <dt class="matchheight"><img src="<?php bloginfo('template_url'); ?>/images/staff_photo3.jpg"  /></dt>
            <dd class="matchheight">
                <h3>□□<span class="job">□□担当</span></h3>
                <p>テキストテキストテキスト<br>
                テキストテキストテキストテキストテキスト<br>
                テキストテキストテキストテキストテキスト</p>
            </dd>
        </dl>
        <dl class="cf mt_l enter-bottom mb_l">
            <dt class="matchheight"><img src="<?php bloginfo('template_url'); ?>/images/staff_photo4.jpg"  /></dt>
            <dd class="matchheight">
                <h3>□□<span class="job">□□担当</span></h3>
                <p>テキストテキストテキスト<br>
                テキストテキストテキストテキストテキスト<br>
                テキストテキストテキストテキストテキスト</p>
            </dd>
        </dl>
    </div>
    <!-- wrapper -->
</section>

<!--
<section class="recruit bg_green pt_l pb_l">
    <div class="wrapper enter-bottom">
        <h3>STAFF募集</h3>
        <div class="outer">
            <div class="inner cf">
                <div class="left">
                    <p>一緒に働いてくれる仲間を募集します！</p>
                    <h4 class="pb_s">スタッフ募集<span class="small">について</span></h4>
                    <dl class="cf">
                        <dt><span class="gray">時給</span></dt>
                        <dd>○,○○○円～</dd>
                    </dl>
                    <dl class="cf">
                        <dt><span class="gray">時間</span></dt>
                        <dd>○○：○○～○○：○○</dd>
                    </dl>
                    <dl class="cf">
                        <dt><span class="gray">資格</span></dt>
                        <dd>○○○○○○○○○○○○○○○○</dd>
                    </dl>
                    <dl class="cf">
                        <dt><span class="gray">待遇</span></dt>
                        <dd>○○○○○○○○○○○○○○○○</dd>
                    </dl>
                </div>
                <div class="right">
                    <img src="<?php bloginfo('template_url'); ?>/images/staff_recruit_photo.jpg"  />
                </div>
            </div>

            <div class="contact cf">
                <div class="title">
                    お問い合わせ・応募はこちら
                </div>
                <div class="tel">
                    <span class="num">011-776-3222</span><span class="time">受付時間：10:00～20:00</span>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- recruit -->

<!-- line_cont -->
<div class="obi"></div>

